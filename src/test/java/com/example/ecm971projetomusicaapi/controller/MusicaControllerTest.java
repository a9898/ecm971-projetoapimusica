package com.example.ecm971projetomusicaapi.controller;

import com.example.ecm971projetomusicaapi.models.Musica;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class MusicaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void TestaOrdenacao() throws Exception{

        // Cadastrar as três músicas da tabela de exemplo
        Musica musica1 = new Musica();
        musica1.setNome("Undone thing");
        musica1.setAvaliacao(5);
        musica1.setId(1L);
        Musica musica2 = new Musica();
        musica2.setNome("One");
        musica2.setAvaliacao(4);
        musica2.setId(2L);
        Musica musica3 = new Musica();
        musica3.setNome("Still");
        musica3.setAvaliacao(3);
        musica3.setId(3L);

        ObjectWriter objectWriter = new ObjectMapper().writer();

        String resultadoEsperado = "{\"id\":1,\"nome\":\"Undone thing\",\"avaliacao\":5},{\"id\":2,\"nome\":\"One\",\"avaliacao\":4},{\"id\":3,\"nome\":\"Still\",\"avaliacao\":3}";

        // Armazenando primeira musica
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/musica/cadastrar")
                                .content(objectWriter.writeValueAsString(musica1))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        // Armazenando Segunda musica
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/musica/cadastrar")
                                .content(objectWriter.writeValueAsString(musica2))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        // Armazenando Terceira musica
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/musica/cadastrar")
                                .content(objectWriter.writeValueAsString(musica3))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        // Avaliando primeira musica
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/musica/avaliar")
                                .content(objectWriter.writeValueAsString(musica1))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        // Avaliando Segunda musica
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/musica/avaliar")
                                .content(objectWriter.writeValueAsString(musica2))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        // Avaliando Terceira musica
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/musica/avaliar")
                                .content(objectWriter.writeValueAsString(musica3))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        // Testando ordem
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get("/musica")
                )
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(resultadoEsperado)));


    }


    @Test
    public void cadastraMusica() throws Exception{

        // Cadastrar musica
        Musica musica1 = new Musica();
        musica1.setNome("Undone thing");
        musica1.setAvaliacao(0);

        String resultadoEsperado = new StringBuilder()
                .append("Nome.: ")
                .append(musica1.getNome())
                .append(", ID.: 1")
                .append(", Avaliacao.: ")
                .append(musica1.getAvaliacao())
                .toString();


        ObjectWriter objectWriter = new ObjectMapper().writer();

        // Armazenando primeira musica
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/musica/cadastrar")
                                .content(objectWriter.writeValueAsString(musica1))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(resultadoEsperado)));


    }

}