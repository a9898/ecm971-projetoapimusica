package com.example.ecm971projetomusicaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ecm971ProjetoMusicaApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ecm971ProjetoMusicaApiApplication.class, args);
    }

}
