package com.example.ecm971projetomusicaapi.repository;

import com.example.ecm971projetomusicaapi.models.Musica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface MusicaRepository extends JpaRepository<Musica, Long> {
}
