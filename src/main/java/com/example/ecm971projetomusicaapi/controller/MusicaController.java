package com.example.ecm971projetomusicaapi.controller;

import com.example.ecm971projetomusicaapi.models.Musica;
import com.example.ecm971projetomusicaapi.service.MusicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/musica")
public class MusicaController {

    @Autowired
    private MusicaService musicaService;

    @PostMapping("/cadastrar")
    public String cadastrar (@RequestBody Musica musica){
        return this.musicaService.salvar(musica);
    }

    @GetMapping
    public List<Musica> listar(){
        return this.musicaService.listar();
    }

    // ToDo.: Write avaliar
    @PostMapping("/avaliar")
    public String avaliar (@RequestBody Musica musica){
        return this.musicaService.atualizar(musica);
    }
}
