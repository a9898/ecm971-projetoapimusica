package com.example.ecm971projetomusicaapi.models;

import lombok.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Musica {
    @Id
    @GeneratedValue
    private Long id;
    private String nome;
    private int avaliacao;
}
