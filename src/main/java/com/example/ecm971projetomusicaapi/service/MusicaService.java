package com.example.ecm971projetomusicaapi.service;

import com.example.ecm971projetomusicaapi.models.Musica;
import com.example.ecm971projetomusicaapi.repository.MusicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MusicaService {

    @Autowired
    private MusicaRepository musicaRepository;

    public String salvar (Musica musica){

        if (musica.getNome().equals("")){
            return "Faltando nome da musica";
        }

        if (jaExisteMusicaPorNome(musica)){
            return "Musica previamente cadastrada";
        }

        int size = this.musicaRepository.findAll().size();
        musica.setAvaliacao(0);
        this.musicaRepository.save(musica);
        return musicaDetalhesCriada(this.musicaRepository.findAll().get(size));
    }

    public List<Musica> listar(){
        return this.musicaRepository.findAll(Sort.by(Sort.Direction.DESC, "avaliacao")
                .and(Sort.by(Sort.Direction.ASC, "nome")));
    }
    
    public String atualizar (Musica musica){
        if (!jaExisteMusicaPorNome(musica)){
            return "Error.: Musica não registrada";
        }

        Musica toupdate = getMusicaPorNome(musica);
        if (musica.getAvaliacao() > 0 && musica.getAvaliacao() < 6){
            toupdate.setAvaliacao(musica.getAvaliacao());
            this.musicaRepository.save(toupdate);
            return "Musica avaliada com sucesso.";
        } else {
            return "Error.: Apenas valores entre 1 e 5";
        }
    }

    private boolean jaExisteMusicaPorNome(Musica musica){
        for (Musica item: this.musicaRepository.findAll()) {
            if (item.getNome().equals(musica.getNome())){
                return true;
            }
        }
        return false;
    }

    private Musica getMusicaPorNome(Musica musica){
        for (Musica item: this.musicaRepository.findAll()) {
            if (item.getNome().equals(musica.getNome())){
                return item;
            }
        }
        return null;
    }

    private String musicaDetalhesCriada(Musica musica){
        return new StringBuilder()
                .append("Nome.: ")
                .append(musica.getNome())
                .append(", ID.: ")
                .append(musica.getId())
                .append(", Avaliacao.: ")
                .append(musica.getAvaliacao())
                .toString();
    }
}
